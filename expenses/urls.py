"""
URL configuration for expenses project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/5.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from receipts.urls import home
from django.shortcuts import redirect


def redirect_receipt(request):#this redircts  http://localhost:8000/ to name of the path for the list view (home.html)
    return redirect('home')#need to import redirect

urlpatterns = [
    path('accounts/', include('accounts.urls')),
    path('', redirect_receipt, name='home'),#this is the redirect path from the function above
    path('receipts/', include('receipts.urls')),
    path("admin/", admin.site.urls),
]

from django import forms
from django.forms import ModelForm


class LogInForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(
        max_length=150,
        widget = forms.PasswordInput)


class SignUpForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput(),
    )
    password_confirmation = forms.CharField(#this is used when you sign up for something and they confirm your password by typing it in again
        max_length=150,
        widget = forms.PasswordInput(),
    )

from django.shortcuts import render, redirect
from accounts.forms import LogInForm, SignUpForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User


# Create your views here.
def user_login(request):
    if request.method == "POST":#if it is an input from the site
        form = LogInForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)#if they put something in the form then run it through authenticate
                return redirect("home")#go back to home template
    else:
        form = LogInForm()
    context = {
        "form": form,
    }
    return render(request, "accounts/login.html", context)


def user_logout(request):
    logout(request)#uses the built in logout
    return redirect('login')#returns you to the login path in urls.y




#Create a view function that will show the signup form for an HTTP GET,
#and try to create a new user for an HTTP POST.
def user_signup(request):
    if request.method == 'POST':
        form=SignUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            confirm = form.cleaned_data['password_confirmation']
            if password == confirm:
                user=User.objects.create_user(username, password=password)#create_method comes from User
                login(request, user)#To log a user in, from a view, use login(). It takes an HttpRequest object
                                    #and a User object. login() saves the user’s ID in the session, using Django’s session framework.
                return redirect('home')#redirect to the list of recipes home.html
            else:
                form.add_error('password', 'the passwords do not match')#else statement for if the password input doesnt match password
    else:
        form = SignUpForm()
    context = {
        'form': form,
    }
    return render(request, 'accounts/signup.html', context)

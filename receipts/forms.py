from django import forms
from receipts.models import Receipt, ExpenseCategory, Account

class ReceiptForm(forms.ModelForm):#allows someone to enter these fields in this form subbmission to create a new Receipt

    class Meta:
        model = Receipt#connected to a model which is connected to the database
        fields = (
            "vendor",
            "total",
            "tax",
            "date",
            "category",
            "account",
        )#next it goes to the views in order to be sent back to the client


class CategoryForm(forms.ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = ('name',)


class AccountForm(forms.ModelForm):
    class Meta:
        model = Account
        fields = (
            'name',
            'number',
        )

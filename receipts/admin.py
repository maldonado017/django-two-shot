from django.contrib import admin
from receipts.models import ExpenseCategory, Account, Receipt#import them from models.py
# Register your models here.
@admin.register(ExpenseCategory)#template to add a model and be able to see it on the admin site
class ExpenseCategoryAdmin(admin.ModelAdmin):
    list_display = [
        'name',
        'owner',
    ]


@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    list_display = [
        'name',
        'number',
        'owner',
    ]

@admin.register(Receipt)
class ReceiptAdmin(admin.ModelAdmin):
    list_display = [
        'vendor',
        'total',
        'tax',
        'date',
        'purchaser',
        'category',
        'account',
    ]

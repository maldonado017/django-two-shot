from django.urls import path
from receipts.views import home, create_receipt, create_category, create_account, category_list, account_list



urlpatterns = [
    path("accounts/", account_list, name="account_list"),
    path("categories/", category_list, name="category_list"),
    path("categories/create/", create_category, name="create_category"),
    path("accounts/create/", create_account, name="create_account"),
    path('create/', create_receipt, name='create_receipt'),
    path('', home, name='home'),
]

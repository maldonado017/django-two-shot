from django.db import models
from django.contrib.auth.models import User#use this to import the User model


# Create your models here.
class ExpenseCategory(models.Model):#a value that we can apply to receipts like 'gas' or 'entertainment
    name = models.CharField(max_length=50)#attributes to the instance in ExpenseCategory
    owner = models.ForeignKey(#shares information with the User model due to ForeignKey
        User,
        related_name = 'categories', #what it shares with User
        on_delete = models.CASCADE,#if you delete the author you delete all the books
    )


class Account(models.Model):#the way that someone paid for something, like a credit card of bank account
    name = models.CharField(max_length=100)
    number = models.CharField(max_length=20)
    owner = models.ForeignKey(
        User,
        related_name = 'accounts',
        on_delete = models.CASCADE,
    )


class Receipt(models.Model):#primary thing that this app keeps track of for accounting purposes#
    vendor = models.CharField(max_length= 200)
    total = models.DecimalField(max_digits=10, decimal_places=3)
    tax = models.DecimalField(max_digits=10, decimal_places=3)
    date = models.DateTimeField()#this may need to be empty or have what I have in scrumptious
    purchaser = models.ForeignKey(
        User,
        related_name = 'receipts',
        on_delete = models.CASCADE,
    )
    category = models.ForeignKey(
        ExpenseCategory,
        related_name = 'receipts', #this info is linked to Expense Cat.  meaning the name and owner will come from there
        on_delete = models.CASCADE,
    )
    account = models.ForeignKey(
        Account,
        related_name = 'receipts',
        on_delete = models.CASCADE,
        null=True,
        blank=True,
    )


#if you need to register to the admin, look at admin.py

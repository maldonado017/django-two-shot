# Generated by Django 5.0 on 2023-12-14 22:42

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("receipts", "0001_initial"),
    ]

    operations = [
        migrations.AlterField(
            model_name="expensecategory",
            name="name",
            field=models.CharField(max_length=50),
        ),
        migrations.AlterField(
            model_name="receipt",
            name="accounts",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                related_name="receipts",
                to="receipts.account",
            ),
        ),
    ]

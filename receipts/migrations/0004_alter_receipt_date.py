# Generated by Django 5.0 on 2023-12-15 02:47

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("receipts", "0003_rename_accounts_receipt_account"),
    ]

    operations = [
        migrations.AlterField(
            model_name="receipt",
            name="date",
            field=models.DateTimeField(),
        ),
    ]
